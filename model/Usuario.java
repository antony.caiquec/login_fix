package model;

public class Usuario {
    private int id;
    private String nome;
    private String senha;
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getNome() {
        return nome;
    }
    public void setNome(String nome) {
        if (nome != null && nome.length() > 3) {
            this.nome = nome;
        }
    }
    public String getSenha() {
        return senha;
    }
    public void setSenha(String senha) {
        if (senha != null && senha.length() > 6) {
            this.senha = senha;
        }
    }
    
}
