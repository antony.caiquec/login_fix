package controller;
import model.Usuario;
import java.util.InputMismatchException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JOptionPane;

public class UsuarioC {
    public boolean login(Usuario user){
        Conexao con = new Conexao();
        Statement ps = null;
        try {
            con.abrirConexao();
            ps = con.getCon().createStatement();
            String nome =  user.getNome();
            String senha = user.getSenha();
            ResultSet rs = ps.executeQuery("SELECT * FROM usuario WHERE nome="+nome+" AND senha="+senha+";");
            if(rs.next()) {
                return true;
            }else{
                return false;
            }
        }catch(SQLException e){
            JOptionPane.showMessageDialog(null, "Erro ao consultar o banco \n Digite com aspas simples!");
            return false;
        }catch(InputMismatchException e){
            System.out.println("Usuario fdp n digita direito");
            return false;
        }finally{
            con.fecharConexao();
        }
    }    
}
