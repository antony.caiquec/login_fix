package controller;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.swing.JOptionPane;

public class Conexao {
    private Connection con = null;
    public Connection getCon() {
        return this.con;
    }
    public void setCon(Connection con) {
        if (con != null) {
            this.con = con;
        }
    }
    public boolean abrirConexao() {
        try{
            Class.forName("com.mysql.cj.jdbc.Driver");
            String usuario = "root";
            String senha = "nfr5tbwicwnHAdugrf74)e_4";
            String banco = "banco";
            String host = "jdbc:mysql://localhost:3306/"+banco;
            this.setCon(DriverManager.getConnection(host, usuario, senha));
            return true;
        }catch(ClassNotFoundException e){
            JOptionPane.showMessageDialog(null, "[-] Drive de conexão não pode ser instanciado.");
            return false;
        }catch(SQLException e){
            JOptionPane.showMessageDialog(null, "[-] Erro ao conectar com o banco de dados. \n Erro: " +e.getMessage());
            return false;
        }
    }
    public boolean fecharConexao(){
        try{
            this.con.close();
            return true;
        }catch(SQLException e){
            System.out.println("[-] Conexão não pode ser fechada.");
            System.out.println("Erro: "+e.getMessage());
            return false;
        }
    }    
}
